/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health;

import android.app.Application;

import com.ingenic.health.model.SpeechModel;
import com.ingenic.health.model.StepCountModel;
import com.ingenic.health.utils.Global;

public class HealthApplication extends Application {

    /**
     * 健康数据传输UUID
     */
    // private static String m_uuid = "461fd36c-ab50-11e4-bac2-5404a6abe086";

    @Override
    public void onCreate() {
        super.onCreate();
        Global.init(this);
        new StepCountModel(this, null).register();
        SpeechModel.getInstance(this);
    }

}
