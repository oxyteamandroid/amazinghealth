/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ingenic.health.model.HeartRateModel;
import com.ingenic.iwds.utils.IwdsLog;

public class HealthReceiver extends BroadcastReceiver {

    public static final String HEALTH_SERVICE_IS_KILLED = "ingenic.intent.action.HEALTH_SERVICE_IS_KILLED";

    @Override
    public void onReceive(Context context, Intent intent) {
        // 启动后台服务
        IwdsLog.d(this, "HealthService start! ---> broadcast ---> " + intent.getAction());
        context.startService(new Intent(context, HealthService.class));
        HeartRateModel.start(context, intent);
    }
}
