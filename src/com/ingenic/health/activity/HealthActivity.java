/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.ingenic.health.R;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;

public class HealthActivity extends RightScrollActivity implements OnRightScrollListener {

    public static final String TAG_SP = "health";
    public static final String TAG_TYPE = "type";

    public static final int TAG_RUN = 0x1;
    public static final int TAG_WALK = 0x2;
    public static final int TAG_CYCLING = 0x4;
    public static final int TAG_INDOOR = 0x8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        iniOnClickEvent();
    }

    private void iniOnClickEvent() {
        OnClickListener listener = new OnClickListener() {

            @Override
            public void onClick(View v) {

                int type = TAG_RUN;
                switch (v.getId()) {
                case R.id.run:
                    type = TAG_RUN;
                    break;
                case R.id.walk:
                    type = TAG_WALK;
                    break;
                case R.id.cycling:
                    type = TAG_CYCLING;
                    break;

                // -------------------------------------------------------------------------------------

                case R.id.history:
                    startActivity(new Intent(HealthActivity.this, HistoryActivity.class));
                    return;
                case R.id.setting:
                    startActivity(new Intent(HealthActivity.this, SettingActivity.class));
                    return;
                }

                startSportActivity(type);
            }

        };

        findViewById(R.id.run).setOnClickListener(listener);
        findViewById(R.id.walk).setOnClickListener(listener);
        findViewById(R.id.cycling).setOnClickListener(listener);

        findViewById(R.id.history).setOnClickListener(listener);
        findViewById(R.id.setting).setOnClickListener(listener);
    }

    private void startSportActivity(int type) {
        Intent intent = new Intent(HealthActivity.this, SportActivity.class);
        intent.putExtra(TAG_TYPE, type);
        startActivity(intent);
    }
}