/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ingenic.health.R;
import com.ingenic.health.db.SportDbOperation;
import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.health.utils.SportDataUtil;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AmazingViewPager;

public class HistoryActivity extends RightScrollActivity {
    private AmazingViewPager mViewPager;
    private LayoutInflater mInflater;
    private SportDbOperation mSportDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history);

        mSportDB = new SportDbOperation(this);

        mInflater = getLayoutInflater();
        mViewPager = (AmazingViewPager) findViewById(R.id.amazing_viewpager);
        mViewPager.setTouchOrientation(AmazingViewPager.VERTICAL);
        mViewPager.setCircularEnabled(false);

        initViewPager();
    }

    private TextView getEmptyView() {
        TextView tv = new TextView(this);
        tv.setText(R.string.sport_no_record);
        tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    private void initViewPager() {
        final List<View> list = new ArrayList<View>();
        final List<SportDataEntity> listEntity = mSportDB.queryHistory(7);
        View last = null;
        for (SportDataEntity entity : listEntity) {
            View journey = SportDataUtil.getWalkHistory(mInflater, null);
            journey.setLayoutParams(SportDataUtil.getLayoutParams());
            list.add(journey);
            last = journey;
            SportDataUtil.fillHistoryView(journey, entity);
        }
        if (null != last)
            last.findViewById(R.id.pull).setVisibility(View.GONE);
        if (list.isEmpty()) {
            list.add(getEmptyView());
        }
        BaseAdapter adapter = new BaseAdapter() {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return list.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public Object getItem(int position) {
                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }
        };
        mViewPager.setAdapter(adapter);
    }

}
