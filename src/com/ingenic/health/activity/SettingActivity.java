/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.ingenic.health.R;
import com.ingenic.health.utils.Global;
import com.ingenic.iwds.app.RightScrollActivity;

public class SettingActivity extends RightScrollActivity {

    private final static String GESTURE_STATE = "gesture_on";

    protected Toast mToast;
    private ImageView mLight;
    private ImageView mVoice;
    private ImageView mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.setting);

        mLight = (ImageView) findViewById(R.id.light_icon);
        mVoice = (ImageView) findViewById(R.id.voice_icon);
        mSave = (ImageView) findViewById(R.id.save_icon);

        OnClickListener clickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                case R.id.parent_light:
                    Intent intents = new Intent("com.ingenic.settings.SUBSETTING");
                    intents.setClassName("com.ingenic.settings", "com.ingenic.settings.SubSettings");
                    intents.putExtra("fragment", "com.ingenic.settings.GestureSettings");
                    startActivity(intents);

                    break;
                case R.id.parent_voice:
                    Global.setVoice(!Global.getVoice());
                    break;
                case R.id.parent_save:
                    Global.setSave(!Global.getSave());
                    break;
                }
                refresh();
            }
        };

        findViewById(R.id.parent_light).setOnClickListener(clickListener);
        findViewById(R.id.parent_voice).setOnClickListener(clickListener);
        findViewById(R.id.parent_save).setOnClickListener(clickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isLight = false;
        try {
            isLight = Settings.System.getInt(getContentResolver(), GESTURE_STATE) > 0;
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }
        Global.setLight(isLight);
        refresh();
    }

    private void refresh() {
        mLight.setImageResource(getImageId(Global.getLight()));
        mVoice.setImageResource(getImageId(Global.getVoice()));
        mSave.setImageResource(getImageId(Global.getSave()));
    }

    private int getImageId(boolean isSelected) {
        return isSelected ? R.drawable.ic_track_selected3 : R.drawable.ic_track_unselect3;
    }
}
