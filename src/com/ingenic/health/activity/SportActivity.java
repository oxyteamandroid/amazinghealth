/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

import com.ingenic.health.R;
import com.ingenic.health.db.SportDbOperation;
import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.health.model.BaseModel.HeartRate2Model;
import com.ingenic.health.model.Model;
import com.ingenic.health.model.SpeechModel;
import com.ingenic.health.model.StepModel;
import com.ingenic.health.model.TimeModel;
import com.ingenic.health.model.TimeModel.TimeListener;
import com.ingenic.health.model.UVModel;
import com.ingenic.health.utils.Global;
import com.ingenic.health.utils.SportDataUtil;
import com.ingenic.health.widget.HealthSportWidgetProvider;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AmazingViewPager;
import com.ingenic.iwds.widget.RightScrollView;

public class SportActivity extends RightScrollActivity {

    public static final String TAG = SportActivity.class.getSimpleName();

    private static final long VIBRATE_DURATION = 200L;

    // 1 新,2继续,3运动,4结束,5保存
    private static final int status_new = 1;
    private static final int status_continue = 2;
    private static final int status_countDown = 3;
    private static final int status_sport = 4;
    private static final int status_finish = 5;
    private static final int status_save = 6;
    private static final int status_exit = -1;

    private int flow = status_new;

    private LayoutInflater mInflater;
    private Handler mHandler;

    private RightScrollView mRightScrollView;

    private View mJourney;
    private View mOneLoop;
    private View mHeartRate;

    private TextView mTiming;

    private int mSportType;
    private SportDataEntity mCurrentData;
    private TimeModel mTimeModel;

    private SportDbOperation mSportDB;
    private StepModel mStepModel;
    private HeartRate2Model mHeartRateModel;

    private UVModel mJourneyUvModel;
    private UVModel mOneLoopUvModel;
    private UVModel mHeartRateUvModel;

    private View startView;
    private View countDownView;
    private View continueView;
    private AmazingViewPager sportView;
    private View endView;
    private View saveView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.sport);
        mRightScrollView = getRightScrollView();
        mRightScrollView.disableRightScroll();

        mInflater = getLayoutInflater();
        mHandler = new Handler();

        mSportDB = new SportDbOperation(this);
        mSportType = getIntent().getIntExtra(HealthActivity.TAG_TYPE, Global.TAG_WALK);

        mCurrentData = new SportDataEntity();
        mCurrentData.type = mSportType;

        initViews();

        setStatus(status_new);
        mCurrentData.setOnCompleteOneLoopListener(new Model.Listener() {

            @Override
            public void onChange(int count, SensorEvent event) {
                IwdsLog.d(TAG, "vibrate yes!!");
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(VIBRATE_DURATION);
                SpeechModel.getInstance(SportActivity.this).speech(SportActivity.this, getString(R.string.sport_complete_one_loop));
            }
        });
    }

    @Override
    protected void onDestroy() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        unregister();
        setStatus(status_exit);
        super.onDestroy();
    }

    public void setStatus(int status) {
        flow = status;
        IwdsLog.d(TAG, "setStatus: " + flow);
        mRightScrollView.disableRightScroll();
        switch (flow) {
        case status_new:
            mRightScrollView.enableRightScroll();
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(startView);
            break;
        case status_continue:
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(continueView);
            break;
        case status_countDown:
            mRightScrollView.enableRightScroll();
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(countDownView);
            startTiming(3);
            break;
        case status_sport:
            register();
            mRightScrollView.enableRightScroll();
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(sportView);
            break;
        case status_finish:
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(endView);
            break;
        case status_save:
            if (mCurrentData.distance < 10) {
                finish();
                return;
            }

            if (Global.getSave()) {
                saveData();
                finish();
                return;
            }
            SportDataUtil.fillMainView(saveView, mCurrentData);
            mRightScrollView.removeAllViews();
            mRightScrollView.addView(saveView);
            break;
        }
    }

    private void unregister() {

        if (null != mOneLoopUvModel) {
            mJourneyUvModel.unregister();
            mOneLoopUvModel.unregister();
            mHeartRateUvModel.unregister();
        }

        if (null != mTimeModel) {
            mTimeModel.unregisterSec();
            mStepModel.unregister();
            mHeartRateModel.unregister();
        }
    }

    public void refresh() {
        IwdsLog.d(TAG, "onRefresh");
        SportDataUtil.fillMainView(mJourney, mCurrentData);
        SportDataUtil.fillOneLoopView(mOneLoop, mCurrentData);
        SportDataUtil.fillHeartRateView(mHeartRate, mCurrentData);
    }

    public void initViews() {

        startView = mInflater.inflate(R.layout.confirm, mRightScrollView, false);
        countDownView = mInflater.inflate(R.layout.timing, mRightScrollView, false);// or
        continueView = mInflater.inflate(R.layout.confirm, mRightScrollView, false);
        sportView = new AmazingViewPager(this);

        mJourney = SportDataUtil.getWalkJourney(mInflater, null);
        mJourney.setLayoutParams(SportDataUtil.getLayoutParams());
        mJourneyUvModel = SportDataUtil.registerUvTextView(this, mJourney, R.id.uv);

        mOneLoop = SportDataUtil.getWalkOneLoop(mInflater, null);
        mOneLoop.setLayoutParams(SportDataUtil.getLayoutParams());
        mOneLoopUvModel = SportDataUtil.registerUvTextView(this, mOneLoop, R.id.uv);

        mHeartRate = SportDataUtil.getWalkHeartRate(mInflater, null);
        mHeartRate.setLayoutParams(SportDataUtil.getLayoutParams());
        mHeartRate.findViewById(R.id.pull).setVisibility(View.GONE);
        mHeartRateUvModel = SportDataUtil.registerUvTextView(this, mHeartRate, R.id.uv);

        endView = mInflater.inflate(R.layout.confirm, mRightScrollView, false);
        saveView = SportDataUtil.getWalkSave(mInflater, mRightScrollView);

        mTiming = (TextView) countDownView.findViewById(R.id.timing);

        fillView(startView, getString(R.string.sport_start), getString(R.string.sport_return), new Listener() {

            @Override
            public void onConfirm(View view) {
                setStatus(status_countDown);
            }

            @Override
            public void onCancel(View view) {
                finish();
            }
        });

        fillView(continueView, getString(R.string.sport_continue), getString(R.string.sport_finish), new Listener() {

            @Override
            public void onConfirm(View view) {
                setStatus(status_sport);
            }

            @Override
            public void onCancel(View view) {
                setStatus(status_save);
            }
        });

        fillView(endView, getString(R.string.sport_pause), getString(R.string.sport_finish), new Listener() {

            @Override
            public void onConfirm(View view) {
                unregister();
                setStatus(status_continue);
            }

            @Override
            public void onCancel(View view) {
                unregister();
                setStatus(status_save);
            }
        });

        saveView.findViewById(R.id.nonsave).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveView.findViewById(R.id.save).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                saveData();
                finish();
            }
        });

        mTimeModel = new TimeModel(this, new TimeListener() {

            @Override
            public void onTimeChange(long time) {
                IwdsLog.d(TAG, "time is change!");
                mCurrentData.caculateElapseTime();
                refresh();
            }
        });
        mHeartRateModel = new HeartRate2Model(this, new Model.Listener() {

            @Override
            public void onChange(int count, SensorEvent value) {
                IwdsLog.d(TAG, "heart rate  is " + count);
                mCurrentData.measureHeartRate(count);
                refresh();
            }
        });

        mStepModel = new StepModel(this, new Model.Listener() {

            @Override
            public void onChange(int count, SensorEvent value) {
                IwdsLog.d(TAG, "step is " + count);
                mCurrentData.add(count);
                refresh();
            }
        });

        FrameLayout.LayoutParams lParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        sportView.setLayoutParams(lParams);
        sportView.setTouchOrientation(AmazingViewPager.VERTICAL);
        sportView.setCircularEnabled(false);
        final List<View> list = new ArrayList<View>();

        list.add(mJourney);
        if ((mSportType & HealthActivity.TAG_WALK) != HealthActivity.TAG_WALK) {
            list.add(mOneLoop);
        }
        list.add(mHeartRate);

        BaseAdapter adapter = new BaseAdapter() {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return list.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public Object getItem(int position) {
                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }
        };
        sportView.setAdapter(adapter);
    }

    public void fillView(final View view, String confirmText, String cancelText, final Listener listener) {
        TextView proceed = (TextView) view.findViewById(R.id.proceed);
        TextView finish = (TextView) view.findViewById(R.id.finish);
        proceed.setText(confirmText);
        finish.setText(cancelText);

        proceed.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.onConfirm(view);
            }
        });

        finish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.onCancel(view);
            }
        });
    }

    public static interface Listener {

        public void onConfirm(View view);

        public void onCancel(View view);
    }

    @Override
    public void onBackPressed() {
        switch (flow) {

        case status_new:
            finish();
            return;

        case status_countDown:
        case status_sport:
            setStatus(status_finish);
            return;
        }
    }

    protected void startTiming(final int time) {

        if (status_countDown != flow)
            return;

        if (time == -1) {
            setStatus(status_sport);
            SpeechModel.getInstance(this).end();
            SpeechModel.getInstance(this).end2();
            return;
        }

        if (0 == time) {
            SpeechModel.getInstance(this).start2(this);
        } else {
            SpeechModel.getInstance(this).start(this);
        }

        mTiming.setText("" + time);
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                startTiming(time - 1);
            }
        }, 1000 * 1);
    }

    private void register() {
        mCurrentData.start();
        mTimeModel.registerSec();
        mHeartRateModel.register();
        mStepModel.register();
    }

    protected void saveData() {
        // IwdsLog.d(TAG, mCurrentData.toString());
        mSportDB.save(mCurrentData);
        HealthSportWidgetProvider.notifyChange();
    }

    @Override
    public void onRightScroll() {
        IwdsLog.d(TAG, "onRight --> flow: " + flow);
        switch (flow) {

        case status_new:
            finish();
            return;

        case status_countDown:
        case status_sport:
            mRightScrollView.scrollTo(0, 0);
            mRightScrollView.setAlpha(1.0f);
            setStatus(status_finish);
            return;
        }
    }
}