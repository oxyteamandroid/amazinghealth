/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.db;

import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.health.entity.StepDataEntity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HealthSQLiteOpenHelper extends SQLiteOpenHelper {

    // 版本号
    private final static int VERSION = 29;
    private static final String DATABASENAME = "health";

    private static HealthSQLiteOpenHelper mInstance = null;

    public static HealthSQLiteOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            synchronized (HealthSQLiteOpenHelper.class) {
                if (mInstance == null) {
                    mInstance = new HealthSQLiteOpenHelper(context.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    public HealthSQLiteOpenHelper(Context context) {
        super(context, DATABASENAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SportDataEntity.createTableSql());
        db.execSQL(StepDataEntity.createTableSql());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + Table.sport + ";");
        db.execSQL("drop table if exists " + Table.steps + ";");
        onCreate(db);
    }

    public static final class Table {
        public static final String sport = "sport";
        public static final String steps = "steps";
    }
}