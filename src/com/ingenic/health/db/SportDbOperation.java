/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ingenic.health.db.HealthSQLiteOpenHelper.Table;
import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.health.entity.StepDataEntity;

/*
 * 主线程操作，不加锁
 */
public class SportDbOperation {

    private SQLiteDatabase db;
    private Cursor cursor;
    private static final String removeEntitySql = "delete from " + Table.sport + " where id = ?";
    private static final String queryHistorySql = "select * from " + Table.sport + " order by id desc limit ?";
    private static final String saveSql = "insert or replace into " + Table.sport
                    + "(id, "
                    + "type, "
                    + "status, "
                    + "distance, "
                    + "time, "
                    + "speed, "
                    + "calorie, "
                    + "oneLoopDistance, "
                    + "oneLoopTime, "
                    + "oneLoopSpeed, "
                    + "heartRate, "
                    + "heartRateInterval, "
                    + "averageHeartRate, "
                    + "startTime, "
                    + "steps, "
                    + "cycles, "
                    + "measureCount, "
                    + "maxHeartRate, "
                    + "totalHeartRate) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    public SportDbOperation(Context context) {
        if (null == db) {
            HealthSQLiteOpenHelper sqLiteOpenHelper = HealthSQLiteOpenHelper.getInstance(context);
            db = sqLiteOpenHelper.getWritableDatabase();
        }
    }

    public long insert(SportDataEntity entity) {

        ContentValues cv = new ContentValues();
        cv.put("type",               entity.type);
        cv.put("status",             entity.status);
        cv.put("distance",           entity.distance);
        cv.put("time",               entity.time);
        cv.put("speed",              entity.speed);
        cv.put("calorie",            entity.calorie);
        cv.put("oneLoopDistance",    entity.oneLoopDistance);
        cv.put("oneLoopTime",        entity.oneLoopTime);
        cv.put("oneLoopSpeed",       entity.oneLoopSpeed);
        cv.put("heartRate",          entity.heartRate);
        cv.put("heartRateInterval",  entity.heartRateInterval);
        cv.put("averageHeartRate",   entity.averageHeartRate);
        cv.put("startTime",          entity.startTime);
        cv.put("steps",              entity.steps);
        cv.put("cycles",             entity.cycles);
        cv.put("measureCount",       entity.measureCount);
        cv.put("maxHeartRate",       entity.maxHeartRate);
        cv.put("totalHeartRate",     entity.totalHeartRate);
        return db.insert(Table.sport, null, cv);
    }

    public void save(SportDataEntity entity) {
        if(null == entity)
            return;
        if(-1L == entity.id){
            entity.id = insert(entity);
            return;
        }
        String[] selectionArgs = new String[] {
                "" + entity.id,
                "" + entity.type,
                "" + entity.status,
                "" + entity.distance,
                "" + entity.time,
                "" + entity.speed,
                "" + entity.calorie,
                "" + entity.oneLoopDistance,
                "" + entity.oneLoopTime,
                "" + entity.oneLoopSpeed,
                "" + entity.heartRate,
                "" + entity.heartRateInterval,
                "" + entity.averageHeartRate,
                "" + entity.startTime,
                "" + entity.steps,
                "" + entity.cycles,
                "" + entity.measureCount,
                "" + entity.maxHeartRate,
                "" + entity.totalHeartRate
        };
        db.execSQL(saveSql, selectionArgs);
    }

    public List<SportDataEntity> queryHistory(int count) {
        cursor = db.rawQuery(queryHistorySql, new String[] { count + "" });
        List<SportDataEntity> list = new ArrayList<SportDataEntity>();
        while (cursor.moveToNext()) {
            list.add(fillEntity(cursor));
        }
        return list;
    }

    public SportDataEntity load(long mIsExists) {
        String selection = "id = ?";
        String[] selectionArgs = new String[] { mIsExists + "" };
        cursor = db.query(Table.sport, null, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) {
            SportDataEntity entity = fillEntity(cursor);
            cursor.close();
            return entity;
        }
        cursor.close();
        return null;
    }


    private SportDataEntity fillEntity(Cursor cursor) {
        SportDataEntity entity = new SportDataEntity();
        entity.id                = cursor.getLong(cursor.getColumnIndex("id"));
        entity.type              = cursor.getInt(cursor.getColumnIndex("type"));
        entity.status            = cursor.getInt(cursor.getColumnIndex("status"));
        entity.distance          = cursor.getInt(cursor.getColumnIndex("distance"));
        entity.time              = cursor.getLong(cursor.getColumnIndex("time"));
        entity.speed             = cursor.getLong(cursor.getColumnIndex("speed"));
        entity.calorie           = cursor.getInt(cursor.getColumnIndex("calorie"));
        entity.oneLoopDistance   = cursor.getInt(cursor.getColumnIndex("oneLoopDistance"));
        entity.oneLoopTime       = cursor.getLong(cursor.getColumnIndex("oneLoopTime"));
        entity.oneLoopSpeed      = cursor.getLong(cursor.getColumnIndex("oneLoopSpeed"));
        entity.heartRate         = cursor.getInt(cursor.getColumnIndex("heartRate"));
        entity.heartRateInterval = cursor.getInt(cursor.getColumnIndex("heartRateInterval"));
        entity.averageHeartRate  = cursor.getInt(cursor.getColumnIndex("averageHeartRate"));
        entity.startTime         = cursor.getLong(cursor.getColumnIndex("startTime"));
        entity.steps             = cursor.getInt(cursor.getColumnIndex("steps"));
        entity.cycles            = cursor.getInt(cursor.getColumnIndex("cycles"));
        entity.measureCount      = cursor.getInt(cursor.getColumnIndex("measureCount"));
        entity.maxHeartRate      = cursor.getInt(cursor.getColumnIndex("maxHeartRate"));
        entity.totalHeartRate    = cursor.getInt(cursor.getColumnIndex("totalHeartRate"));
        return entity;
    }

    public void revome(SportDataEntity entity) {
        revome(entity.id);
    }

    public void revome(long id) {
        if (-1 == id)
            return;
        db.execSQL(removeEntitySql, new String[] { "" + id });
    }

    // ===============================================================================================================================
    
    public long insert(StepDataEntity entity) {
        ContentValues cv = new ContentValues();
        cv.put("steps",              entity.steps);
        cv.put("date",               entity.date);
        return db.insert(Table.steps, null, cv);
    }
 
    public long save(StepDataEntity entity) {
        if(null == entity)
            return -1;
        if(-1L == entity.id){
            entity.id = insert(entity);
            return entity.id;
        }

        String saveSql = "insert or replace into " + Table.steps
                + "(id, "
                + "steps, "
                + "date) "
                + "values (?, ?, ?)";

        
        String[] selectionArgs = new String[] {
                "" + entity.id,
                "" + entity.steps,
                "" + entity.date,
        };
        db.execSQL(saveSql, selectionArgs);
        return 0;
    }

    public Cursor getCursor(long date) {
        String selection = "date = ?";
        String[] selectionArgs = new String[] { date + "" };
        return db.query(Table.steps, null, selection, selectionArgs, null, null, null);
    }

    public StepDataEntity query(long date) {
        String selection = "date = ?";
        String[] selectionArgs = new String[] { date + "" };
        cursor = db.query(Table.steps, null, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) {
            StepDataEntity entity = fillStepEntity(cursor);
            cursor.close();
            return entity;
        }
        cursor.close();
        return null;
    }

    public List<StepDataEntity> loadAll() {
        cursor = db.query(Table.steps, null, null, null, null, null, null);
        List<StepDataEntity> list = new ArrayList<StepDataEntity>();
        if (cursor.moveToNext()) {
            StepDataEntity entity = fillStepEntity(cursor);
            list.add(entity);
        }
        cursor.close();
        return list;
    }

    private StepDataEntity fillStepEntity(Cursor cursor) {
        StepDataEntity entity = new StepDataEntity();
        entity.id                = cursor.getLong(cursor.getColumnIndex("id"));
        entity.steps             = cursor.getInt(cursor.getColumnIndex("steps"));
        entity.date              = cursor.getLong(cursor.getColumnIndex("date"));
        return entity;
    }
}
