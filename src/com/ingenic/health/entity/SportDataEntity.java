/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.entity;

import android.util.SparseIntArray;

import com.ingenic.health.db.HealthSQLiteOpenHelper.Table;
import com.ingenic.health.model.Model.Listener;
import com.ingenic.health.utils.Global;
import com.ingenic.health.utils.Util;
import com.ingenic.iwds.utils.IwdsLog;

public class SportDataEntity {

    public static final String TAG = SportDataEntity.class.getSimpleName();

    private static final int ONE_LOOP_DISTANCE = 1000;

    private static final SparseIntArray map = Global.map;
    private static final SparseIntArray resMap = Global.resMap;

    private static final long MINUTE_59 = 1000 * (3600) - 1;
    private static final long HOUR_23 = 1000 * (3600) * 24 - 1;

    public int getImageResId() {
        return resMap.get(type);
    }

    public long id = -1L;

    public int type; // 跑步，徒步，骑行 ＊ 室内 ＝ 6个
    public int status; // 完成，暂停，进行中

    public int distance;
    public long time;
    public long speed;
    public int calorie;

    public int oneLoopDistance;
    public long oneLoopTime;
    public long oneLoopSpeed;

    public int heartRate;
    public int heartRateInterval;
    public int averageHeartRate;

    public long startTime;

    public int steps;
    public int cycles; // 圈数

    public int measureCount;
    public int maxHeartRate;
    public int totalHeartRate;

    private CaculateTimeHelper mTimeHelper;
    private Listener listener;

    public void setOnCompleteOneLoopListener(Listener listener) {
        this.listener = listener;
    }

    public void add(int step) {
        steps += step;
        IwdsLog.d(TAG, "steps: " + steps + "step: " + step);
        caculateSteps();
        // IwdsLog.d(TAG, toString());
    }

    private void caculateSteps() {
        distance = (int) calculateMileage(steps);
        speed = 0 == distance ? 0 : (long) (((double) time * 1000) / distance);
        speed = speed > MINUTE_59 ? MINUTE_59 : speed;
        calorie = (int) calculateCalorie(steps);
        oneLoopDistance = distance % ONE_LOOP_DISTANCE;

        caculateElapseTime();
        oneLoopSpeed = 0 == oneLoopDistance ? 0 : (long) (((double) oneLoopTime * 1000) / oneLoopDistance);
        oneLoopSpeed = oneLoopSpeed > MINUTE_59 ? MINUTE_59 : oneLoopSpeed;
    }

    public static float calculateMileage(float steps) {
        int height = 170;
        return (steps) * (float) ((height) / 6.875 * 3) / 100;
    }

    public static float calculateCalorie(float steps) {
        float stepsmin;

        int height = 170;
        int weight = 120;
        stepsmin = 70;

        double th = 0;
        double tw = 0;
        if ((height) >= 175)
            th = 175;
        else
            th = 0.8 * (height) + 40;
        if ((weight) >= 65)
            tw = 65;
        else
            tw = 0.4 * (weight) + 40;

        double temp = (0.43 * (height) + 0.57 * (weight) + 0.26 * stepsmin + 0.92 * 10/* min */- 108.44 * th / 175 * tw / 65) / 10 / 60 * steps;
        if (temp < 0)
            temp = 0;

        return (float) temp;
    }

    public void caculateElapseTime() {

        mTimeHelper.stop();
        time += mTimeHelper.getElapse();
        time = time > HOUR_23 ? HOUR_23 : time;
        cycles = 0 == cycles ? distance / ONE_LOOP_DISTANCE : cycles;

        if (cycles > 0 && cycles * ONE_LOOP_DISTANCE <= distance) {
            oneLoopTime = 0;
            cycles++;
            if (null != listener)
                listener.onChange(cycles, null);
        }
        oneLoopTime += mTimeHelper.getElapse();
        oneLoopTime = oneLoopTime > HOUR_23 ? HOUR_23 : oneLoopTime;
        mTimeHelper.start();
    }

    public void measureHeartRate(int rate) {
        caculateHeartRate(rate);
        // IwdsLog.d(TAG, toString());
    }

    private void caculateHeartRate(int rate) {
        measureCount++;
        heartRate = rate;
        maxHeartRate = maxHeartRate > rate ? maxHeartRate : rate;
        totalHeartRate += rate;
        averageHeartRate = totalHeartRate / measureCount;
        int tempHeartRate = (averageHeartRate * 100) / maxHeartRate;
        if (tempHeartRate < 60)
            heartRateInterval = 1;
        else if (tempHeartRate < 70)
            heartRateInterval = 2;
        else if (tempHeartRate < 80)
            heartRateInterval = 3;
        else if (tempHeartRate < 90)
            heartRateInterval = 4;
        else if (tempHeartRate < 100)
            heartRateInterval = 5;
        caculateSteps();
    }

    // ====================================================================================================================

    public static class CaculateTimeHelper {
        public long now;
        public long lastTime;
        private long elapse;

        public void start() {
            if (0L == now)
                now = System.currentTimeMillis();
            lastTime = now;
        }

        public void stop() {
            now = System.currentTimeMillis();
            elapse = now - lastTime;
        }

        public long getElapse() {
            return elapse;
        }
    }

    // ====================================================================================================================

    public int getTypeStr() {
        return map.get(type);
    }

    // ====================================================================================================================
    public void start() {
        mTimeHelper = new CaculateTimeHelper();
        mTimeHelper.start();
        if (startTime == 0L)
            startTime = mTimeHelper.now;
    }

    // ====================================================================================================================
    @Override
    public String toString() {
        String str = "============================================================"
                + "\r\nid:                 " + id
                + "\r\ntype:               " + type
                + "\r\nstatus:             " + status
                + "\r\ndistance:           " + distance
                + "\r\ntime:               " + Util.getFormatMinute(time)
                + "\r\nspeed:              " + speed
                + "\r\ncalorie:            " + calorie
                + "\r\noneLoopDistance:    " + oneLoopDistance
                + "\r\noneLoopTime:        " + Util.getFormatMinute(oneLoopTime)
                + "\r\noneLoopSpeed:       " + oneLoopSpeed
                + "\r\nheartRate:          " + heartRate
                + "\r\nheartRateInterval:  " + heartRateInterval
                + "\r\naverageHeartRate:   " + averageHeartRate
                + "\r\nstartTime:          " + Util.getFormatDate(startTime)
                + "\r\nsteps:              " + steps
                + "\r\ncycles:             " + cycles
                + "\r\nmeasureCount:       " + measureCount
                + "\r\nmaxHeartRate:       " + maxHeartRate
                + "\r\ntotalHeartRate:     " + totalHeartRate
                + "\r\n============================================================";
        return str;
    }

    public static String createTableSql() {
        String sql = "create table if not exists " + Table.sport
                + "("
                + "id  integer  primary key  autoincrement , "
                + "type                   integer not null , "
                + "status                 integer not null , "
                + "distance               integer not null , "
                + "time                   integer not null , "
                + "speed                  integer not null , "
                + "calorie                integer not null , "
                + "oneLoopDistance        integer not null , "
                + "oneLoopTime            integer not null , "
                + "oneLoopSpeed           integer not null , "
                + "heartRate              integer not null , "
                + "heartRateInterval      integer not null , "
                + "averageHeartRate       integer not null , "
                + "startTime              integer not null , "
                + "steps                  integer not null , "
                + "cycles                 integer not null , "
                + "measureCount           integer not null , "
                + "maxHeartRate           integer not null , "
                + "totalHeartRate         integer not null ) ";
        return sql;
    }
}