/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.entity;

import com.ingenic.health.db.HealthSQLiteOpenHelper.Table;
import com.ingenic.health.utils.Util;

public class StepDataEntity {

    public static final String TAG = StepDataEntity.class.getSimpleName();

    public long id = -1L;
    public int steps;
    public long date;

    // ====================================================================================================================

    @Override
    public String toString() {
        String str = "============================================================"
                + "\r\nid:                 " + id
                + "\r\nsteps:              " + steps
                + "\r\ndate:               " + Util.getFormatDay(date)
                + "\r\n============================================================";
        return str;
    }

    public static String createTableSql(){
        String sql = "create table if not exists " + Table.steps + "("
                + "id  integer  primary key  autoincrement , "
                + "steps                  integer not null , "
                + "date                   integer not null ) ";
        return sql;
    }
}