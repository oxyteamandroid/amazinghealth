/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.util.SparseArray;

import com.ingenic.health.model.Model.Listener;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;

public class BaseModel {

    private static final SparseArray<Set<Listener>> repos = new SparseArray<Set<Listener>>();
    private static final SparseArray<Listener> repo = new SparseArray<Listener>();

    private Model baseModel;
    private Listener listener;

    private Listener defaultListener;
    private Set<Listener> listeners;

    public BaseModel(Context context, int sensorType, Listener listener) {
        this.baseModel = Model.getInstance(context, sensorType);
        this.listeners = getListeners(sensorType);
        defaultListener = getListener(sensorType);
        this.listener = null == listener ? Listener.empty : listener;
    }

    private Set<Listener> getListeners(int sensorType) {
        Set<Listener> result = repos.get(sensorType);
        if (null == result) {
            result = new HashSet<Model.Listener>();
            repos.put(sensorType, result);
        }
        return result;
    }

    private Listener getListener(int type) {
        Listener ls = repo.get(type);
        if (null == ls) {
            ls = new Listener() {

                @Override
                public void onChange(int count, SensorEvent event) {
                    handle(count, event);
                }
            };
            repo.put(type, ls);
        }
        return ls;
    }

    protected void handle(int count, SensorEvent event) {
        call(count, event);
    }

    protected void call(int count, SensorEvent event) {
        for (Listener item : listeners) {
            item.onChange(count, event);
        }
    }

    public void register() {
        listeners.add(listener);
        baseModel.register(defaultListener);
    }

    public void unregister() {
        if (listeners.contains(listener))
            listeners.remove(listener);
        if (listeners.isEmpty())
            baseModel.unregister(defaultListener);
    }

    // =========================================================================

    public static class PressureModel extends BaseModel {

        public PressureModel(Context context, Listener listener) {
            super(context, Sensor.TYPE_PRESSURE, listener);
        }
    }

    public static class HeartRate2Model extends BaseModel {

        public HeartRate2Model(Context context, Listener listener) {
            super(context, Sensor.TYPE_HEART_RATE, listener);
        }
    }
}