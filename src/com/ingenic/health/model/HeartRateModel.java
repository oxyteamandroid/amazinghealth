/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import android.content.Context;
import android.content.Intent;

import com.ingenic.health.model.Model.Listener;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;

public class HeartRateModel extends BaseModel {

    private static final String DATA_ACTION = "ingenic.intent.action.HEART_REATE_DATA";
    private static final String OPERATE_ACTION = "ingenic.intent.action.HEART_REATE_OPERATE";

    protected static final String DATA_HEART_RATE_TAG = "data_heart_rate_tag";
    protected static final String OPERATE_TYPE_TAG = "type_tag";
    protected static final int TYPE_START = 0;
    protected static final int TYPE_END = 1;
    protected static final String TAG = HeartRateModel.class.getSimpleName();

    private static Context sContext;
    private static HeartRateModel sModel;

    public HeartRateModel(Context context, Listener listener) {
        super(context, Sensor.TYPE_HEART_RATE, listener);
    }

    public static void start(Context context, Intent intent) {
        if (!intent.getAction().equals(OPERATE_ACTION))
            return;
        int type = intent.getIntExtra(OPERATE_TYPE_TAG, -1);
        if (type == TYPE_START) {
            startMeasuring(context);
        } else if (type == TYPE_END) {
            stopMeasuring();
            sendStopBroadcast(context);
        }
    }

    public static void startMeasuring(Context context) {

        if (null != sModel)
            return;

        sContext = context.getApplicationContext();
        sModel = new HeartRateModel(sContext, new Listener() {

            @Override
            public void onChange(int count, SensorEvent event) {
                IwdsLog.d(TAG, "onChange : " + count);
                Intent intent = new Intent(DATA_ACTION);
                intent.putExtra(DATA_HEART_RATE_TAG, count);
                sContext.sendBroadcast(intent);
            }
        });
        sModel.register();
    }

    public static void stopMeasuring() {
        if (null == sContext)
            return;
        sModel.unregister();

        sContext = null;
        sModel = null;

    }

    public static void sendStopBroadcast(Context context) {
        Intent intent = new Intent(DATA_ACTION);
        intent.putExtra(OPERATE_TYPE_TAG, TYPE_END);
        context.sendBroadcast(intent);
    }

}
