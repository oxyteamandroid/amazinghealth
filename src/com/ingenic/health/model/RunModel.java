/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import java.util.Arrays;

import android.content.Context;

import com.ingenic.health.model.Model.Listener;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;

public class RunModel extends BaseModel {

    static final String TAG = RunModel.class.getSimpleName();

    public RunModel(Context context, Listener listener) {
        super(context, Sensor.TYPE_MOTION, listener);
    }

    @Override
    protected void handle(int count, SensorEvent event) {

        float[] mMotionData = new float[2];
        mMotionData[0] = event.values[0];
        mMotionData[1] = event.values[1];
        long mMotionTime = event.timestamp;

        int data = (int) mMotionData[0];
        int tossAndTurn = (int) mMotionData[1];

        IwdsLog.d(TAG, "Motion:\n" + motionRawDataToString(data) + "\ntossAndTurn_cnt: " + tossAndTurn + "\ntimestamp: " + mMotionTime);
        IwdsLog.d(TAG, "Motion:\n" + motionRawDataToString(data) + Arrays.toString(event.values));

        if (data == SensorEvent.MOTION_RUN) {
            call(count, event);
        }
    }

    private String motionRawDataToString(int data) {
        String str;
        if (data == SensorEvent.MOTION_RESET) {
            str = "Rest-->code: ";
        } else if (data == SensorEvent.MOTION_STOP) {
            str = "Stop-->code: ";
        } else if (data == SensorEvent.MOTION_WALK) {
            str = "Walk-->code: ";
        } else if (data == SensorEvent.MOTION_RUN) {
            str = "Run-->code: ";
        } else if (data == SensorEvent.MOTION_SLEEP) {
            str = "SLEEP-->code: ";
        } else if (data == SensorEvent.MOTION_DEEP_SLEEP) {
            str = "DEEP_SLEEP-->code: ";
        } else {
            str = "unknown-->code: ";
        }
        str = str + data;
        return str;
    }
}
