/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.ingenic.health.R;
import com.ingenic.health.utils.Global;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartspeech.RemoteSpeechConstant;
import com.ingenic.iwds.smartspeech.RemoteSpeechErrorCode;
import com.ingenic.iwds.smartspeech.RemoteSpeechServiceManager;
import com.ingenic.iwds.smartspeech.RemoteSpeechSynthesizer;
import com.ingenic.iwds.smartspeech.RemoteSpeechSynthesizer.RemoteSynthesizerListener;
import com.ingenic.iwds.smartspeech.RemoteStatusListener;
import com.ingenic.iwds.utils.IwdsLog;

public class SpeechModel implements ConnectionCallbacks, RemoteSynthesizerListener, RemoteStatusListener {

    public static final String TAG = SpeechModel.class.getSimpleName();

    private static SpeechModel sInstance;

    private ServiceClient mClient;
    private RemoteSpeechSynthesizer mSpeaker;
    private RemoteSpeechServiceManager mService;
    private int mStatus = RemoteSpeechErrorCode.ERROR_REMOTE_DISCONNECTED;

    private MediaPlayer mp;

    private MediaPlayer mp2;

    private SpeechModel(Context context) {
        IwdsLog.d(TAG, "speech servcie ---> SpeechModel: ");
        mClient = new ServiceClient(context, ServiceManagerContext.SERVICE_REMOTE_SPEECH, this);
        // mClient.connect();
    }

    public static SpeechModel getInstance(Context context) {
        if (null == sInstance)
            synchronized (SpeechModel.class) {
                if (null == sInstance)
                    sInstance = new SpeechModel(context);
            }
        return sInstance;
    }

    public void speech(Context context, String text) {
        if (!Global.getVoice())
            return;
        if (!isAvailable()) {
            // Toast.makeText(context, "未连接!", Toast.LENGTH_SHORT).show();
            IwdsLog.d(TAG, "not link ");
            return;
        }

        mSpeaker.setText(text);
        if (!mService.requestStartSpeak(mSpeaker))
            // Toast.makeText(context, "请求失败!", Toast.LENGTH_SHORT).show();
            IwdsLog.d(TAG, "request fails");
        return;
    }

    private void setParameters() {
        mSpeaker.setParameter(RemoteSpeechConstant.ENGINE_TYPE, "local");
        mSpeaker.setParameter(RemoteSpeechSynthesizer.SPEED, "50");
        mSpeaker.setParameter(RemoteSpeechSynthesizer.PITCH, "50");
        mSpeaker.setParameter(RemoteSpeechSynthesizer.VOLUME, "20");
        mSpeaker.setParameter(RemoteSpeechSynthesizer.VOICE_NAME, "xiaoyan");
    }

    public boolean isAvailable() {
        return RemoteSpeechErrorCode.SUCCESS == mStatus;
    }

    // ============================================================================================

    @Override
    public void onConnected(ServiceClient serviceClient) {
        IwdsLog.d(TAG, "speech servcie ---> onConnected: ");
        mSpeaker = RemoteSpeechSynthesizer.getInstance();
        mSpeaker.setListener(this);
        mService = (RemoteSpeechServiceManager) mClient.getServiceManagerContext();
        mService.registerRemoteStatusListener(this);
        setParameters();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.d(TAG, "speech servcie ---> onDisconnected: ");
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsLog.d(TAG, "speech servcie ---> onConnectFailed: ");
    }

    // ============================================================================================

    @Override
    public void onSpeakingStatus(boolean isSpeaking) {
        IwdsLog.d(TAG, "speech servcie ---> onSpeakingStatus: " + isSpeaking);
    }

    @Override
    public void onSynthCompleted(int errorCode) {
        IwdsLog.d(TAG, "speech servcie ---> onSynthCompleted: " + errorCode);
    }

    @Override
    public void onSpeakBegin() {
        IwdsLog.d(TAG, "speech servcie ---> onSpeakBegin: ");
    }

    @Override
    public void onSpeakPaused() {
        IwdsLog.d(TAG, "speech servcie ---> onSpeakPaused: ");
    }

    @Override
    public void onSynthProgress(int progress) {
        IwdsLog.d(TAG, "speech servcie ---> onSynthProgress: " + progress);
    }

    @Override
    public void onSpeakResumed() {
        IwdsLog.d(TAG, "speech servcie ---> onSpeakResumed: ");
    }

    @Override
    public void onError(int errorCode) {
        IwdsLog.d(TAG, "speech servcie ---> onError: " + errorCode);
    }

    @Override
    public void onCancel() {
        IwdsLog.d(TAG, "speech servcie ---> onCancel: ");
    }

    // ============================================================================================

    @Override
    public void onAvailable(int errorCode) {
        mStatus = errorCode;
        IwdsLog.d(TAG, "speech servcie ---> onAvailable: " + isAvailable());
    }

    public void start(Context context) {
        if (!Global.getVoice())
            return;
        mp = null;
        mp = null != mp ? mp : MediaPlayer.create(context, R.raw.beep);
        mp.start();
        mp.setOnCompletionListener(listener);
        IwdsLog.d(TAG, "start voice !");
    }

    public void end() {
        if (!Global.getVoice() || null == mp)
            return;
        // mp.stop();
        // mp.release();
        mp = null;
    }

    private OnCompletionListener listener = new OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            mp.stop();
            mp.release();
        }
    };

    public void start2(Context context) {
        if (!Global.getVoice())
            return;
        mp2 = null;
        mp2 = null != mp2 ? mp2 : MediaPlayer.create(context, R.raw.beep2);
        mp2.setOnCompletionListener(listener);
        mp2.start();
        IwdsLog.d(TAG, "start voice 2!");
    }

    public void end2() {
        if (!Global.getVoice() || null == mp2)
            return;
        // mp2.stop();
        // mp2.release();
        mp2 = null;
    }
}