/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import java.util.HashSet;
import java.util.Set;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ingenic.health.db.SportDbOperation;
import com.ingenic.health.entity.StepDataEntity;
import com.ingenic.health.model.Model.Listener;
import com.ingenic.health.utils.DateUtils;
import com.ingenic.iwds.smartsense.SensorEvent;

public class StepCountModel {

    private static StepDataEntity entity;
    private static SportDbOperation db;

    private static final Set<RefreshListener> listeners = new HashSet<RefreshListener>();

    public static void start(Context context) {
        if (null != db) {
            return;
        }
        new StepModel(context, new Listener() {

            @Override
            public void onChange(int count, SensorEvent value) {
                changeStepsCount(count);
            }
        }).register();

        db = new SportDbOperation(context);

        long time = System.currentTimeMillis();
        long date = DateUtils.getDateByLong(time);
        entity = getEntity(date);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        /* intentFilter.addAction(Intent.ACTION_TIME_TICK); */

        context.registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                changeStepsCount(0);
            }
        }, intentFilter);
    }

    public static StepDataEntity getEntity(long date) {
        StepDataEntity entity = db.query(date);
        if (null == entity) {
            entity = new StepDataEntity();
            entity.date = date;
        }
        return entity;
    }

    public static void changeStepsCount(int count) {
        long time = System.currentTimeMillis();
        long date = DateUtils.getDateByLong(time);

        if (entity.date != date) {
            entity = getEntity(date);
        }

        if (entity.steps != 0) {
            db.save(entity);
        }

        entity.steps += count;

        for (RefreshListener item : listeners)
            item.onRefresh(entity);
    }

    private RefreshListener mListener;
    private Context mContext;

    public StepCountModel(Context context, RefreshListener listener) {
        mContext = context;
        mListener = null == listener ? RefreshListener.empty : listener;
    }

    public void register() {
        start(mContext);
        listeners.add(mListener);
    }

    public void unregister() {
        listeners.remove(mListener);
    }

    public static interface RefreshListener {

        public void onRefresh(StepDataEntity entity);

        public static final RefreshListener empty = new RefreshListener() {

            @Override
            public void onRefresh(StepDataEntity entity) {
            }
        };
    }
}