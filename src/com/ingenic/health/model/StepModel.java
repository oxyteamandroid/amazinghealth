/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import android.content.Context;

import com.ingenic.health.model.Model.Listener;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;

public class StepModel extends BaseModel {

    public StepModel(Context context, Listener listener) {
        super(context, Sensor.TYPE_STEP_COUNTER, listener);
    }

    private static int sLastSensorSteps;
    private static int sCurrentSensorSteps;
    private static int sStepsCount;

    @Override
    protected void handle(int count, SensorEvent event) {
        sCurrentSensorSteps = count;
        sLastSensorSteps = (0 == sLastSensorSteps) ? Math.max(0, sCurrentSensorSteps - 6) : sLastSensorSteps;
        sLastSensorSteps = (sLastSensorSteps > sCurrentSensorSteps) ? sCurrentSensorSteps : sLastSensorSteps;
        sStepsCount = sCurrentSensorSteps - sLastSensorSteps;
        sLastSensorSteps = sCurrentSensorSteps;

        // IwdsLog.d(RunModel.TAG, "Motion:\n steps: " + Arrays.toString(event.values));

        call(sStepsCount, event);
    }
}
