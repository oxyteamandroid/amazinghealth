/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.model;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;

import com.ingenic.health.model.Model.Listener;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;

public class UVModel extends BaseModel {

    private static final Set<UVListener> listeners = new HashSet<UVListener>();
    private static Listener listener = new Listener() {

        @Override
        public void onChange(int count, SensorEvent value) {
            for (UVListener item : listeners) {
                IwdsLog.d("tag","uv.......... " + value.values[0]);
                item.onUVChange(value.values[0], "");
            }
        }
    };

    private UVListener uvListener;

    public UVModel(Context context, UVListener uvListener) {
        super(context, Sensor.TYPE_UV, listener);
        this.uvListener = null == uvListener ? UVListener.empty : uvListener;
    }

    @Override
    public void register() {
        listeners.add(uvListener);
        super.register();
    }

    @Override
    public void unregister() {
        if (listeners.contains(listener))
            listeners.remove(uvListener);
        if (listeners.isEmpty())
            super.unregister();
    }

    public static interface UVListener {

        public void onUVChange(float value, String result);

        public static final UVListener empty = new UVListener() {

            @Override
            public void onUVChange(float value, String result) {
            }
        };
    }
}