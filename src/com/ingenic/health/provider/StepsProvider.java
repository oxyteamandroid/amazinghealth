/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.ingenic.health.db.SportDbOperation;
import com.ingenic.health.entity.StepDataEntity;
import com.ingenic.health.model.StepCountModel;
import com.ingenic.health.model.StepCountModel.RefreshListener;
import com.ingenic.health.utils.DateUtils;

public class StepsProvider extends ContentProvider {

    // private static final String TAG = "MyView";
    private static StepsProvider instance;
    private SportDbOperation db;
    private StepCountModel mStempCountModel;
    private static Uri mUri = Uri.parse("content://com.ingenic.providers.stepsprovider");

    public static void notifyChange() {
        if (null != instance)
            instance.notifyChange(mUri);
    }

    private void notifyChange(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null);
    }

    @Override
    public boolean onCreate() {
        instance = this;
        db = new SportDbOperation(getContext());

        mStempCountModel = new StepCountModel(getContext(), new RefreshListener() {

            @Override
            public void onRefresh(StepDataEntity entity) {
                notifyChange();
            }
        });
        mStempCountModel.register();

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        long date = DateUtils.getDateByLong(System.currentTimeMillis());
        Cursor cursor = db.getCursor(date);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}