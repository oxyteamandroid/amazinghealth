/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 * 
 * @author Jiangyanbo
 * 
 */
public class DateUtils {

    private static Calendar mCalendar = Calendar.getInstance();

    public static String getCurrentDateString() {

        return getCurrentDate() + "";
    }

    public static long getCurrentDate() {

        mCalendar.setTimeInMillis(System.currentTimeMillis());

        // 将当天的时、分、秒、毫秒清0，从而获得年月日的Long型值
        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);

        return mCalendar.getTimeInMillis();
    }

    public static long getDateByLong(long time) {

        mCalendar.setTimeInMillis(time);

        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);

        return mCalendar.getTimeInMillis();
    }

    /**
     * 由于在该程序中当天的计步是以两个小时为一个时间点，因此通过下面来获得当前的时间要归入哪个时间点
     * 
     * @return 2,4...24
     */
    public static int getDateHour(long time) {

        mCalendar.setTimeInMillis(time);

        return (mCalendar.get(Calendar.HOUR_OF_DAY) / 2) * 2 + 2;
    }

    public static int getWeekBy(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(time));
        int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return week;
    }

    public static int getDateHour() {
        mCalendar.setTimeInMillis(System.currentTimeMillis());

        return (mCalendar.get(Calendar.HOUR_OF_DAY) / 2) * 2 + 2;
    }
}