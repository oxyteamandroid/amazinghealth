/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.utils;

import com.ingenic.health.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.SparseArray;
import android.util.SparseIntArray;

public class Global {

    private static final String SETTING = "setting";
    private static final String SAVE = "save";
    private static final String VOICE = "voice";
    private static final String LIGHT = "light";

    private static boolean sLight;
    private static boolean sVoice;
    private static boolean sSave;
    private static SharedPreferences sp;
    private static Editor editor;

    public static final int TAG_RUN = 0x1;
    public static final int TAG_WALK = 0x2;
    public static final int TAG_CYCLING = 0x4;
    public static final int TAG_INDOOR = 0x8;

    public static final SparseIntArray map = new SparseIntArray();
    public static final SparseIntArray resMap = new SparseIntArray();

    public static void init(Context context) {

        initSportStr(context);

        initSetting(context);
    }

    private static void initSportStr(Context context) {
        map.put(TAG_RUN, R.string.sport_running);
        map.put(TAG_WALK, R.string.sport_walking);
        map.put(TAG_CYCLING, R.string.sport_cycling);
        map.put(TAG_INDOOR | TAG_RUN, R.string.sport_indoor_running);
        map.put(TAG_INDOOR | TAG_WALK, R.string.sport_indoor_walking);
        map.put(TAG_INDOOR | TAG_CYCLING, R.string.sport_indoor_cycling);

        resMap.put(TAG_RUN, R.drawable.sport_run);
        resMap.put(TAG_WALK, R.drawable.sport_walk);
        resMap.put(TAG_CYCLING, R.drawable.sport_cycle);
        resMap.put(TAG_INDOOR | TAG_RUN, R.drawable.sport_run_indoor);
        resMap.put(TAG_INDOOR | TAG_WALK, R.drawable.sport_walk_indoor);
        resMap.put(TAG_INDOOR | TAG_CYCLING, R.drawable.sport_cycle_indoor);
    }

    private static void initSetting(Context context) {
        sp = context.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        editor = sp.edit();

        Global.sLight = sp.getBoolean(LIGHT, false);
        Global.sVoice = sp.getBoolean(VOICE, false);
        Global.sSave = sp.getBoolean(SAVE, false);
    }

    public static void setLight(boolean value) {
        Global.sLight = value;
        editor.putBoolean(LIGHT, value);
        editor.commit();
    }

    public static void setVoice(boolean value) {
        Global.sVoice = value;
        editor.putBoolean(VOICE, value);
        editor.commit();
    }

    public static void setSave(boolean value) {
        Global.sSave = value;
        editor.putBoolean(SAVE, value);
        editor.commit();
    }

    public static boolean getLight() {
        return Global.sLight;
    }

    public static boolean getVoice() {
        return Global.sVoice;
    }

    public static boolean getSave() {
        return Global.sSave;
    }

}