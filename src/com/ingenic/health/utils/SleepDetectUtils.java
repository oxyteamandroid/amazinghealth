///*
// *  Copyright (C) 2015 Ingenic Semiconductor
// *
// *  Jiangyanbo <yanbo.jiang@ingenic.com>
// *
// *  Elf/AmazingHealth Project
// *
// *  This program is free software; you can redistribute it and/or modify it
// *  under the terms of the GNU General Public License as published by the
// *  Free Software Foundation; either version 2 of the License, or (at your
// *  option) any later version.
// *
// *  You should have received a copy of the GNU General Public License along
// *  with this program; if not, write to the Free Software Foundation, Inc.,
// *  675 Mass Ave, Cambridge, MA 02139, USA.
// *
// */
//package com.ingenic.health.utils;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
//import android.content.Context;
//
//import com.ingenic.health.db.DbOperation;
//import com.ingenic.iwds.datatransactor.elf.HealthInfo;
//import com.ingenic.iwds.datatransactor.elf.HealthInfo.Record;
//
///**
// * sleep detect 工具类
// * 
// * @author Jiangyanbo
// * 
// */
//public class SleepDetectUtils {
//
//	//private static final String TAG = SleepDetectUtils.class.getSimpleName();
//    private static final int DEFAULT_TARGET_TIME = 8 * 3600 * 1000;
//
//    private static final long HOUR = 1000 * 3600;
//    private static final long DAY = HOUR * 24;
//
//    private List<Record> mRecord = new ArrayList<HealthInfo.Record>();
//    private List<Record> mLastRecords = new ArrayList<HealthInfo.Record>();
//
//    private long mTarget = DEFAULT_TARGET_TIME;
//
//    private int mCurrentType = -1;
//    private Record mCurrentRecord = new Record();
//    private long mDates;
//    private int mEndIndex;
//    private DbOperation mDbOperation;
//
//    public SleepDetectUtils(Context context) {
//        mDbOperation = new DbOperation(context);
//    }
//
//    private long getTime(Record record) {
//        return record.endTime - record.startTime;
//    }
//
//    public long getStartTime() {
//        List<Record> filter = filter(mRecord);
//        if (filter.size() == 0)
//            return 0l;
//        return filter.get(0).startTime;
//    }
//
//    public long getEndTime() {
//        List<Record> filter = filter(mRecord);
//        if (filter.size() == 0)
//            return 0l;
//        return filter.get(filter.size() - 1).endTime;
//    }
//
//    private List<Record> filter(List<Record> record) {
//        return mLastRecords;
//    }
//
//    public void add(int type) {
//
//        long timeStamp = System.currentTimeMillis();
//
//        if (timeStamp > mDates) {
//            mRecord.clear();
//            mDates = DateUtils.getDateByLong(timeStamp);
//            mDates += HOUR * 12;
//            mDates += DAY;
//
//        }
//
//        mCurrentRecord.endTime = timeStamp;
//
//        // 加两小时？
//        if (type != mCurrentType) {
//            mCurrentRecord = new Record();
//            mCurrentRecord.type = type;
//            mCurrentRecord.startTime = mCurrentRecord.endTime = timeStamp;
//
//            if (mCurrentRecord.type == HealthInfo.MOTION_SLEEP || mCurrentRecord.type == HealthInfo.MOTION_DEEP_SLEEP) {
//                if (mRecord.size() == 0) {
//
//                    mDbOperation.saveSleepTimes(mLastRecords, getDate());
//                    mLastRecords.clear();
//                }
//                mRecord.add(mCurrentRecord);
//                mEndIndex = mRecord.size();
//                for (int i = mLastRecords.size(); i < mEndIndex; i++) {
//                    mLastRecords.add(mRecord.get(i));
//                }
//            } else {
//                if (mRecord.size() > 0) {
//                    mRecord.add(mCurrentRecord);
//                }
//            }
//        }
//
//    }
//
//    public long getDate() {
//        List<Record> temp = filter(mRecord);
//        long time = temp.size() == 0 ? System.currentTimeMillis() : getEndTime();
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(time);
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        cal.set(Calendar.MILLISECOND, 0);
//        return cal.getTimeInMillis();
//    }
//
//    public long getSleepTime() {
//        return getDeepSleepTime() + getLightSleepTime();
//    }
//
//    public long getDeepSleepTime() {
//        long temp = 0l;
//        for (Record item : filter(mRecord)) {
//            if (item.type == HealthInfo.MOTION_DEEP_SLEEP)
//                temp += getTime(item);
//        }
//        return temp;
//    }
//
//    public long getLightSleepTime() {
//        long temp = 0l;
//        for (Record item : filter(mRecord)) {
//            if (item.type == HealthInfo.MOTION_SLEEP)
//                temp += getTime(item);
//        }
//        return temp;
//    }
//
//    public long getActiveTime() {
//        long temp = 0l;
//        for (Record item : filter(mRecord)) {
//            if (item.type == HealthInfo.MOTION_RESET)
//                temp += getTime(item);
//        }
//        return temp;
//    }
//
//    public float getSleepQuality() {
//        float temp = getSleepTargetProgress() * 5;
//        temp = Math.min(temp, 5.0f);
//        //IwdsLog.d(TAG, "getSleepQuality = " + temp);
//        return temp;
//    }
//
//    public float getSleepTargetProgress() {
//        long sleepTime = getSleepTime();
//        float result = ((float) sleepTime / (float) mTarget);
//        // IwdsLog.d(TAG, "sleepTime : mTarget => " + sleepTime + " : " + mTarget + " ---> " + (result));
//        return result;
//    }
//
//    public Record[] getSleepRecords() {
//        List<Record> filter = filter(mRecord);
//        Record[] temp = new Record[filter.size()];
//        return filter.toArray(temp);
//    }
//
//}