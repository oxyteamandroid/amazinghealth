/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ingenic.health.R;
import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.health.model.UVModel;
import com.ingenic.health.model.UVModel.UVListener;

public class SportDataUtil {

    private static final int INIT_8_TO_24 = 16 * 3600 * 1000;
    public static Context sContext;

    public static void init(Context context) {
        sContext = context;
    }

    // 1
    public static View getWalkJourney(LayoutInflater inflater, ViewGroup root) {
        View walk = inflater.inflate(R.layout.run, root, false);
        walk.findViewById(R.id.date_record_parent).setVisibility(View.GONE);
        walk.findViewById(R.id.confirm_parent).setVisibility(View.GONE);
        walk.findViewById(R.id.hearts).setVisibility(View.GONE);
        return walk;
    }

    // 1.1
    public static View getWalkOneLoop(LayoutInflater inflater, ViewGroup root) {
        View walk = getWalkJourney(inflater, root);
        walk.findViewById(R.id.calorie).setVisibility(View.INVISIBLE);
        return walk;
    }

    // 2
    public static View getWalkOneLoops(LayoutInflater inflater, ViewGroup root) {
        View walk = getWalkOneLoop(inflater, root);
        walk.findViewById(R.id.speed).setVisibility(View.GONE);
        walk.findViewById(R.id.calorie).setVisibility(View.VISIBLE);
        return walk;
    }

    // 1.1.1
    public static View getWalkHeartRate(LayoutInflater inflater, ViewGroup root) {
        View walk = getWalkOneLoop(inflater, root);
        walk.findViewById(R.id.heart_rate_group).setVisibility(View.GONE);
        return walk;
    }

    // 2.2
    public static View getWalkHistory(LayoutInflater inflater, ViewGroup root) {
        View walk = getWalkOneLoops(inflater, root);
        walk.findViewById(R.id.date_record_parent).setVisibility(View.VISIBLE);
        walk.findViewById(R.id.uv).setVisibility(View.GONE);
        return walk;
    }

    // 2.3
    public static View getWalkSave(LayoutInflater inflater, ViewGroup root) {
        View walk = getWalkOneLoops(inflater, root);
        walk.findViewById(R.id.pull).setVisibility(View.GONE);
        walk.findViewById(R.id.type_parent).setVisibility(View.VISIBLE);
        walk.findViewById(R.id.confirm_parent).setVisibility(View.VISIBLE);
        walk.findViewById(R.id.data_type).setVisibility(View.GONE);
        return walk;
    }

    // 3
    public static ViewGroup.LayoutParams getLayoutParams() {
        return new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    // 上面getView;
    /* ========================================================================================= */
    // 下面fillView;

    public static HolderView fillMainView(View view, SportDataEntity entity) {
        if (null == view)
            return null;
        HolderView holder = getHolderView(view);
        holder.data.setText(getDistanceUnit(entity.distance) + "m");
        holder.time.setText(getFormatMinute(entity.time));
        holder.speed.setText(getFormatSpeed(entity.speed) + "/km");
        holder.calorie.setText(entity.calorie + "cal");
        holder.type.setText(entity.getTypeStr());
        showTimeView(holder, entity);
        return holder;
    }

    public static HolderView fillOneLoopView(View view, SportDataEntity entity) {
        if (null == view)
            return null;
        HolderView holder = getHolderView(view);
        holder.data.setText(getDistanceUnit(entity.oneLoopDistance) + "m");
        holder.time.setText(getFormatMinute(entity.oneLoopTime));

        holder.speed.setText(getFormatSpeed(entity.oneLoopSpeed) + "/km");
        holder.type.setText(entity.getTypeStr());
        holder.dataType.setText(R.string.sport_one_loop);
        showTimeView(holder, entity);
        return holder;
    }

    public static HolderView fillHeartRateView(View view, SportDataEntity entity) {
        if (null == view)
            return null;
        HolderView holder = getHolderView(view);
        holder.data.setText(entity.heartRate + "bpm");
        holder.time.setText(holder.time.getResources().getString(R.string.sport_average_heart_rate) + entity.averageHeartRate + "bpm");
        holder.speed.setText(R.string.sport_heart_rate_level);
        holder.rating.setRating(entity.heartRateInterval);

        holder.type.setText(entity.getTypeStr());
        holder.dataType.setText(R.string.heart_rate);
        showTimeView(holder, entity);
        return holder;
    }

    public static String getDistanceUnit(int distance) {
        if (distance > 1000) {
            float ds = distance / 1000;
            return new DecimalFormat("###.##").format(ds) + "k";
        }
        return new DecimalFormat("###").format(distance);
    }

    public static String getFormatSpeed(long time) {
        return new SimpleDateFormat("m'min'ss's'", Locale.getDefault()).format(new Date(time + INIT_8_TO_24));
    }

    public static String getFormatMinute(long time) {
        return new SimpleDateFormat("HH:mm  ss", Locale.getDefault()).format(new Date(time + INIT_8_TO_24));
    }

    public static String getFormatHours(long time) {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date(time + INIT_8_TO_24));
    }

    public static String getFormatHoursNotAdd8(long time) {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date(time));
    }

    public static String getFormatDay(long time) {
        return new SimpleDateFormat("yyyy/M/d", Locale.getDefault()).format(new Date(time));
    }

    public static String getFormatDate(long time) {
        return new SimpleDateFormat("yyyy/M/d HH:mm:ss", Locale.getDefault()).format(new Date(time));
    }

    public static HolderView fillHistoryView(View view, SportDataEntity entity) {
        if (null == view)
            return null;
        Object ho = view.getTag();
        if (null != ho)
            return (HolderView) ho;

        HolderView holder = fillMainView(view, entity);

        showTimeView(holder, entity);
        holder.dateRecord.setText(getFormatDay(entity.startTime));
        holder.startTime.setText(getFormatHoursNotAdd8(entity.startTime));

        holder.type.setText(R.string.history);
        holder.dataType.setText(entity.getTypeStr());

        holder.time.setText(getFormatMinute(entity.time));
        holder.calorie.setText(entity.calorie + "cal");

        view.setTag(holder);
        return holder;
    }

    private static HolderView showTimeView(HolderView holder, SportDataEntity entity) {
        String template = "HH:mm";
        SimpleDateFormat format = new SimpleDateFormat(template, Locale.getDefault());
        holder.now.setText(format.format(new Date(System.currentTimeMillis())));
        return holder;
    }

    public static HolderView getHolderView(View view) {
        HolderView holderView = null;
        if (null == view.getTag()) {
            holderView = new HolderView();
            holderView.type = (TextView) view.findViewById(R.id.type);
            holderView.dataType = (TextView) view.findViewById(R.id.data_type);

            holderView.data = (TextView) view.findViewById(R.id.data);
            holderView.time = (TextView) view.findViewById(R.id.time);
            holderView.speed = (TextView) view.findViewById(R.id.speed);
            holderView.calorie = (TextView) view.findViewById(R.id.calorie);

            holderView.dateRecord = (TextView) view.findViewById(R.id.date_record);
            holderView.startTime = (TextView) view.findViewById(R.id.start_time);

            holderView.uv = (TextView) view.findViewById(R.id.uv);
            holderView.rating = (RatingBar) view.findViewById(R.id.rating);
            holderView.now = (TextView) view.findViewById(R.id.now);
            view.setTag(holderView);
        }
        return null == holderView ? (HolderView) view.getTag() : holderView;
        // return holderView;
    }

    public static class HolderView {

        public TextView type;
        public TextView dataType;

        public TextView data;
        public TextView time;
        public TextView speed;
        public TextView calorie;

        public TextView dateRecord;
        public TextView startTime;

        public TextView uv;
        public RatingBar rating;
        public TextView now;
    }

    // 下面fillView;
    /* ========================================================================================= */
    // 上面register UV sensor;

    public static UVModel registerUvTextView(Context context, View view, int id) {
        final TextView tv = (TextView) view.findViewById(id);
        UVModel uvModel = new UVModel(context, new UVListener() {

            @Override
            public void onUVChange(float value, String result) {
                tv.setText(result + ": " + value);
            }
        });
        uvModel.register();
        return uvModel;
    }
}
