/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.health.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ingenic.health.model.UVModel;
import com.ingenic.health.model.UVModel.UVListener;

public class Util {

    public static Context sContext;

    public static void init(Context context) {
        sContext = context;
    }

    public static String getFormatMinute(long time) {
        return new SimpleDateFormat("mm:ss", Locale.getDefault()).format(new Date(time));
    }

    public static String getFormatHours(long time) {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date(time));
    }

    public static String getFormatDay(long time) {
        return new SimpleDateFormat("yyyy/M/d", Locale.getDefault()).format(new Date(time));
    }

    public static String getFormatDate(long time) {
        return new SimpleDateFormat("yyyy/M/d HH:mm:ss", Locale.getDefault()).format(new Date(time));
    }

    /* ========================================================================================= */

    public static UVModel registerUvTextView(Context context, View view, int id) {
        final TextView tv = (TextView) view.findViewById(id);
        UVModel uvModel = new UVModel(context, new UVListener() {

            @Override
            public void onUVChange(float value, String result) {
                tv.setText(result + ": " + value);
            }
        });
        uvModel.register();
        return uvModel;
    }

    /* ========================================================================================= */

    public static float calculateCalorie(float steps) {
        float stepsmin;

        int height = 180;
        int weight = 150;
        stepsmin = 70;

        double th = 0;
        double tw = 0;
        if ((height) >= 175)
            th = 175;
        else
            th = 0.8 * (height) + 40;
        if ((weight) >= 65)
            tw = 65;
        else
            tw = 0.4 * (weight) + 40;

        double temp = (0.43 * (height) + 0.57 * (weight) + 0.26 * stepsmin + 0.92 * 10/* min */- 108.44 * th / 175 * tw / 65) / 10 / 60 * steps;
        if (temp < 0)
            temp = 0;

        return (float) temp;
    }

    public static float calculateMileage(float steps) {
        int height = 170;
        return (steps) * (float) ((height) / 6.875 * 3) / 100;
    }

}
