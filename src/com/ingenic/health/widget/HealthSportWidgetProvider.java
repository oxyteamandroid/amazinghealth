package com.ingenic.health.widget;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.view.View;

import com.ingenic.health.R;
import com.ingenic.health.activity.HealthActivity;
import com.ingenic.health.db.SportDbOperation;
import com.ingenic.health.entity.SportDataEntity;
import com.ingenic.iwds.appwidget.WidgetProvider;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.LocalRemoteViews;
import com.ingenic.iwds.widget.OnClickHandler;

public class HealthSportWidgetProvider extends WidgetProvider {

    protected static final String TAG = HealthSportWidgetProvider.class.getSimpleName();

    private LocalRemoteViews mRemoteViews;
    private SportDbOperation mSportDB;

    private String mPkName;

    private static HealthSportWidgetProvider sHeartRateWidgetProvider;

    @Override
    protected void onEnabled() {
        super.onEnabled();
        IwdsLog.i(this, "Provider is enabled");
        mSportDB = new SportDbOperation(this);
        sHeartRateWidgetProvider = this;
    }

    protected void onDisabled() {
        super.onDisabled();
        sHeartRateWidgetProvider = null;
    }

    public static void notifyChange() {
        if (null != sHeartRateWidgetProvider) {
            sHeartRateWidgetProvider.updateRemoteViews();
        }
    }

    @Override
    protected LocalRemoteViews onCreateRemoteViews() {
        IwdsLog.i(this, "onCreateRemoteViews()");
        if (mRemoteViews == null) {
            IwdsLog.i(this, "create");
            mRemoteViews = new LocalRemoteViews(getPackageName(), R.layout.widget_layout_health);
            updateRemoteViews();
        }
        mRemoteViews.setOnClickHandler(R.id.health_sport, new OnClickHandler() {

            @Override
            protected void onClick(String callingPkg, int viewId) {
                Intent intent = new Intent(getApplicationContext(), HealthActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        return mRemoteViews;
    }

    public void updateRemoteViews() {
        if (null == mRemoteViews)
            return;

        List<SportDataEntity> listEntity = mSportDB.queryHistory(1);
        SportDataEntity entity = listEntity.size() > 0 ? listEntity.get(0) : null;
        fillView(mRemoteViews, entity);
    }

    private void fillView(LocalRemoteViews remoteViews, SportDataEntity entity) {

        if (null == entity || null == remoteViews) {
            remoteViews.setViewVisibility(R.id.no_record, View.VISIBLE);
            remoteViews.setViewVisibility(R.id.sport_container, View.GONE);
            return;
        }
        remoteViews.setViewVisibility(R.id.no_record, View.GONE);
        remoteViews.setViewVisibility(R.id.sport_container, View.VISIBLE);
        remoteViews.setImageViewResource(R.id.sport_type, entity.getImageResId());

        remoteViews.setTextViewText(R.id.start_time, new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(new Date(entity.startTime)));
        remoteViews.setTextViewText(R.id.distance, entity.distance + "");
        remoteViews.setTextViewText(R.id.calorie, entity.calorie + "");
        remoteViews.setTextViewText(R.id.take_time, new SimpleDateFormat("H'h'm'min's's'", Locale.getDefault()).format(new Date(entity.time + 16 * 60 * 60 * 1000)));

        updateWidgetForHost(mPkName, mRemoteViews);
    }

    @Override
    protected void onDeletedFromHost(String hostPkg) {
        super.onDeletedFromHost(hostPkg);
        mPkName = null;
    }

    @Override
    protected void onAddedToHost(String hostPkg) {
        super.onAddedToHost(hostPkg);
        IwdsLog.i(this, "provider is added to host: " + hostPkg);
        mPkName = hostPkg;
    }

    @Override
    protected LocalRemoteViews onUpdate(LocalRemoteViews views) {
        IwdsLog.i(this, "Provider update");
        return views;
    }
}
