package com.ingenic.health.widget;

import java.util.Calendar;

import android.graphics.drawable.AnimationDrawable;
import android.view.View;

import com.ingenic.health.R;
import com.ingenic.health.model.HeartRateModel;
import com.ingenic.health.model.Model.Listener;
import com.ingenic.health.model.TimeModel;
import com.ingenic.health.model.TimeModel.TimeListener;
import com.ingenic.iwds.appwidget.WidgetProvider;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.LocalRemoteViews;
import com.ingenic.iwds.widget.OnClickHandler;

public class HeartRateWidgetProvider extends WidgetProvider {

    protected static final String TAG = HeartRateWidgetProvider.class.getSimpleName();

    private LocalRemoteViews mRemoteViews;
    private AnimationDrawable mAnimation;

    private HeartRateModel mHeartRateModel;
    private long start;
    private int measureHeartRate;
    protected boolean isMeasure;

    private String mPkName;

    protected TimeModel mTimeModel;

    @Override
    protected void onEnabled() {
        super.onEnabled();
        IwdsLog.i(this, "Provider is enabled");
    }

    @Override
    protected LocalRemoteViews onCreateRemoteViews() {
        IwdsLog.i(this, "onCreateRemoteViews()");
        if (mRemoteViews == null) {
            IwdsLog.i(this, "create");
            // mRemoteViews = new LocalRemoteViews(getPackageName(), R.layout.widget_layout_heart_rate);
        }
        getAnim();
        showHeartRate();
        mRemoteViews.setOnClickHandler(0/* R.id.heart_rate_icon */, new OnClickHandler() {

            @Override
            protected void onClick(String callingPkg, int viewId) {

                if ((isMeasure = !isMeasure)) {

                    mHeartRateModel = new HeartRateModel(getApplicationContext(), new Listener() {

                        @Override
                        public void onChange(int count, SensorEvent event) {
                            showHeartRate();

                            measureHeartRate = count;
                            mRemoteViews.setTextViewText(R.id.steps, "" + count);
                            if (null != mPkName) {
                                updateWidgetForHost(mPkName, mRemoteViews);
                            }
                        }
                    });

                    showMeasuring();
                    if (null != mPkName) {
                        updateWidgetForHost(mPkName, mRemoteViews);
                    }
                    mHeartRateModel.register();
                    IwdsLog.d(TAG, "start measure heart rate!");
                } else {
                    mHeartRateModel.unregister();
                    IwdsLog.d(TAG, "stop measure heart rate!");

                    showHeartRate();

                    start = System.currentTimeMillis();
                    updateLastMeasure();
                    mTimeModel = null != mTimeModel ? mTimeModel : new TimeModel(getApplicationContext(), new TimeListener() {

                        @Override
                        public void onTimeChange(long time) {
                            IwdsLog.d(TAG, "pass a minute");
                            updateLastMeasure();
                        }
                    });
                    mTimeModel.registerMin();

                }
            }

            public void updateLastMeasure() {
                // mRemoteViews.setTextViewText(R.id.timestamp, getTimeDescription(start) + " " + measureHeartRate);
                if (null != mPkName) {
                    updateWidgetForHost(mPkName, mRemoteViews);
                }
            }
        });

        return mRemoteViews;
    }

    public void showHeartRate() {
        // mRemoteViews.setViewVisibility(R.id.measuring, View.GONE);
        // mRemoteViews.setViewVisibility(R.id.steps, View.VISIBLE);
        // mRemoteViews.setViewVisibility(R.id.unit, View.VISIBLE);
    }

    public void showMeasuring() {
        // mRemoteViews.setViewVisibility(R.id.measuring, View.VISIBLE);
        // mRemoteViews.setViewVisibility(R.id.steps, View.GONE);
        // mRemoteViews.setViewVisibility(R.id.unit, View.GONE);
    }

    public void getAnim() {
        if (null == mAnimation) {
            mAnimation = (AnimationDrawable) getResources().getDrawable(R.anim.electrocardiogram);
        }
    }

    @Override
    protected void onDeletedFromHost(String hostPkg) {
        super.onDeletedFromHost(hostPkg);
        mPkName = null;
    }

    @Override
    protected void onAddedToHost(String hostPkg) {
        super.onAddedToHost(hostPkg);
        IwdsLog.i(this, "provider is added to host: " + hostPkg);
        mPkName = hostPkg;
    }

    @Override
    protected LocalRemoteViews onUpdate(LocalRemoteViews views) {
        IwdsLog.i(this, "Provider update");
        return views;
    }

    public String getTimeDescription(long start) {
        return "";
    }
}
