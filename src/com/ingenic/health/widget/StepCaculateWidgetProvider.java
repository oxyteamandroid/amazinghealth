package com.ingenic.health.widget;

import android.os.Handler;
import android.text.TextUtils;

import com.ingenic.health.R;
import com.ingenic.iwds.appwidget.WidgetProvider;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.LocalRemoteViews;

public class StepCaculateWidgetProvider extends WidgetProvider {

    protected static final String TAG = StepCaculateWidgetProvider.class.getSimpleName();

    private LocalRemoteViews mRemoteViews;
    private CircleImage mci;
    private Handler mHandler;
    private String mPkName;
    private Thread mThread;
    protected Runnable runnable;

    @Override
    protected void onEnabled() {
        super.onEnabled();
        IwdsLog.i(this, "Provider is enabled");
    }

    @Override
    protected LocalRemoteViews onCreateRemoteViews() {
        IwdsLog.i(this, "onCreateRemoteViews()");
        if (mRemoteViews == null) {
            IwdsLog.i(this, "create");
//            mRemoteViews = new LocalRemoteViews(getPackageName(), R.layout.widget_layout);
            mci = new CircleImage();
            mci.setWidth(100);
            mci.setHeight(100);
            // mci.setPaintWidth(24);
//            mRemoteViews.setImageViewBitmap(R.id.background, mci.getImage());
            mHandler = new Handler();
        }
        if (null != mThread)
            return mRemoteViews;

        mThread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {

                    int j = i % 13;
                    mci.setSweepAngle(j * 30);
                    IwdsLog.d(TAG, "i:  " + j * 3);

                    mHandler.removeCallbacks(runnable);

                    runnable = new Runnable() {
                        @Override
                        public void run() {
//                            mRemoteViews.setImageViewBitmap(R.id.background, mci.getImage());
                            if (!TextUtils.isEmpty(mPkName))
                                updateWidgetForHost(mPkName, mRemoteViews);
                        }
                    };
                    mHandler.post(runnable);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        // mThread.start();

        return mRemoteViews;
    }

    @Override
    protected void onDeletedFromHost(String hostPkg) {
        super.onDeletedFromHost(hostPkg);
        mPkName = null;
    }

    @Override
    protected void onAddedToHost(String hostPkg) {
        super.onAddedToHost(hostPkg);
        IwdsLog.i(this, "provider is added to host: " + hostPkg);
        mPkName = hostPkg;
    }

    @Override
    protected LocalRemoteViews onUpdate(LocalRemoteViews views) {
        IwdsLog.i(this, "Provider update");
        return views;
    }
}
